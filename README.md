# django_challenge    
 Django_challenge API expose schoo API and student API    
    
## Run Service    
 Run local server    
    
 python manage.py runserver    
Run with docker compose    
    
 docker-compose up    
## Deployment Pipeline    
 When there is a changes push to `main` branch, deployment pipeline automatically start with following step  
  
 - start run automate test to verify functionality  
 - build docker image and push to AWS ECR  
 - deploy latest image to AWS ECS cluster  
  
The application is running on ECS Cluster behind Elastic load balancer  
application available at 
**Cluster was deleted due to cost saving**
